package cn.mikylin.core.util.http;

import org.apache.http.*;
import org.apache.http.client.HttpRequestRetryHandler;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.ConnectionKeepAliveStrategy;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HttpContext;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.http.ssl.TrustStrategy;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * 1.http连接池不是万能的,过多的长连接会占用服务器资源,导致其他服务受阻
 * 2.http连接池只适用于请求是经常访问同一主机(或同一个接口)的情况下
 * 3.并发数不高的情况下资源利用率低下
 *
 * @author zrt
 */
public final class HttpClientUtilBrainTest {

    private static Logger logger = LogManager.getLogger(HttpClientUtilBrainTest.class);
    public static final Integer SUCCESS_CODE;
    public static final String POST;
    public static final String GET;
    private static RequestConfig CONFIG;
    private static CloseableHttpClient CLIENT;
    private static String CHARSET;
    private static ScheduledExecutorService monitorExecutor;
    private static long ALIVE_TIME;
    private static long IDLE_TIME;
    private static PoolingHttpClientConnectionManager connecManager;


    static {
        ALIVE_TIME = 10000L;
        IDLE_TIME = 5000L;
        SUCCESS_CODE = 200;
        POST = "POST";
        GET = "GET";
        CHARSET = "UTF-8";
        //请求参数配置，15秒超时
        CONFIG = RequestConfig.custom()
                .setConnectTimeout(15000)//取得连接到连通目标url的连接等待时间
                .setSocketTimeout(15000)//从服务器获取响应数据需要等待的时间
                .setConnectionRequestTimeout(1000)//从连接池中取得连接的时间
                .build();

        /**
         * 增加 httpclient 线程池,
         *配置ssl连接策略，信任所有
         */
        try {
            //配置连接器管理器
            SSLContext sslcontext = new SSLContextBuilder()//ssl上下文
                    .loadTrustMaterial(null,
                            new TrustStrategy() {
                                @Override
                                public boolean isTrusted(X509Certificate[] chain, String authType) throws CertificateException {
                                    return true;
                                }
                            })
                    .build();
            HostnameVerifier hostnameVerifier = NoopHostnameVerifier.INSTANCE;//用于SSL连接中主机名的校验，这里不校验
            SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(////创立SSLSocket连接
                    sslcontext, hostnameVerifier);


            Registry<ConnectionSocketFactory> socketFactoryRegistry = RegistryBuilder.<ConnectionSocketFactory>create()
                    .register("http", PlainConnectionSocketFactory.getSocketFactory())
                    .register("https", sslsf)
                    .build();


            //持久连接策略设置10秒。客户端的keepAlive无效
            ConnectionKeepAliveStrategy keepAliveStrategy = new ConnectionKeepAliveStrategy() {
                @Override
                public long getKeepAliveDuration(HttpResponse response, HttpContext context) {
                    return ALIVE_TIME;
                }
            };


            //失败重发策略
            HttpRequestRetryHandler retryHandler = new HttpRequestRetryHandler() {
                @Override
                public boolean retryRequest(IOException e, int i, HttpContext httpContext) {
                    if (i > 3) {
                        return false;
                    }
                    if (e instanceof NoHttpResponseException) {
                        //服务器没有响应，是服务器断开了连接应该重试
                        logger.error("NoHttpResponseExceptioni：" + i);
                        return true;
                    }

                    HttpClientContext clientContext = HttpClientContext.adapt(httpContext);
                    HttpRequest request = clientContext.getRequest();
                    boolean idempotent = !(request instanceof HttpEntityEnclosingRequest);
                    //Retry if the request is considered idempotent
                    //如果请求类型不是HttpEntityEnclosingRequest，被认为是幂等的，那么就重试
                    //HttpEntityEnclosingRequest指的是有请求体的request，比HttpRequest多一个Entity属性
                    //而常用的GET请求是没有请求体的，POST、PUT都是有请求体的
                    //Rest一般用GET请求获取数据，故幂等，POST用于新增数据，故不幂等
                    if (idempotent) {
                        return true;
                    }
                    return false;
                }
            };


            //初始化连接管理器
            connecManager = new PoolingHttpClientConnectionManager(socketFactoryRegistry);
            connecManager.setMaxTotal(500);//连接池最大连接数
            connecManager.setDefaultMaxPerRoute(50);//路由最大连接数

            CLIENT = HttpClients.custom()
                    .setConnectionTimeToLive(ALIVE_TIME, TimeUnit.MILLISECONDS)
                    .setKeepAliveStrategy(keepAliveStrategy)
                    .setConnectionManager(connecManager)
                    .setRetryHandler(retryHandler)
                    .evictExpiredConnections()//设置这两项，会开启定时任务清理过期和闲置的连接
                    .evictIdleConnections(IDLE_TIME, TimeUnit.MILLISECONDS)
                    .build();

        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }


    }


    /**
     * 发送一个有参的http post请求
     * 参数以json字符串形式传入
     */
    public static String postJsonString(String url, String jsonStr) throws IOException {
        HttpPost post = new HttpPost(url);
        post.setConfig(CONFIG);
        post.setEntity(setStringEntity(jsonStr));
        return post(post);
    }


    private static StringEntity setStringEntity(String jsonStr) {
        StringEntity entity = new StringEntity(jsonStr, CHARSET);//解决中文乱码问题
        entity.setContentType("application/json");
        return entity;
    }


    /**
     * 发送一个有参的http post请求
     * 参数以map形式传入
     */
    public static String postJsonString(String url, Map params) throws IOException {
        HttpPost post = new HttpPost(url);
        post.setConfig(CONFIG);
        post.setEntity(setPostParam(params));
        return post(post);


    }


    /**
     * post后，释放连接
     */
    public static String post(HttpPost post) throws IOException {
        CloseableHttpResponse response = null;
        try {
            response = CLIENT.execute(post);
            HttpEntity entity = response.getEntity();
            return EntityUtils.toString(entity, CHARSET);
        } finally {
            if (response != null) {
                response.close();
            }
        }
    }


    private static StringEntity setPostParam(Map params) throws UnsupportedEncodingException {
        List<NameValuePair> list = new ArrayList<>();
        params.keySet().forEach(
                key -> list.add(new BasicNameValuePair((String) key, (String) params.get(key)))
        );
        return new UrlEncodedFormEntity(list, CHARSET);
    }
}
