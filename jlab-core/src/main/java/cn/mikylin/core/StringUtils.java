package cn.mikylin.core;

import java.util.Objects;

public class StringUtils {

    /**
     * 判断字符串是否是空的，仅仅是 null 和空字符串的效验
     * 是空的 - true
     * 不是空的 - false
     */
    public static Boolean isEmpty(String str){
        if(Objects.isNull(str) || str.isEmpty())
            return Boolean.TRUE;
        return Boolean.FALSE;
    }

    /**
     * 判断字符串是否不是空的，仅仅是 null 和空字符串的效验
     * 是空的 - false
     * 不是空的 - true
     */
    public static Boolean isNotEmpty(String str){
        return !isEmpty(str);
    }

    /**
     * 判断字符串是否是空的，包括了各类换行符等的效验
     * 是空的 - true
     * 不是空的 - false
     */
    public static Boolean isBlank(String str) {
        String trimStr;
        if(isEmpty(trimStr = str.trim()))
            return Boolean.TRUE;

        for (int i = 0; i < trimStr.length(); i ++)
            if (!Character.isWhitespace(str.charAt(i)))
                return Boolean.FALSE;
        return Boolean.TRUE;
    }

    /**
     * 判断字符串是否不是空的，包括了各类换行符等的效验
     * 是空的 - false
     * 不是空的 - true
     */
    public static Boolean isNotBlank(String str){
        return !isBlank(str);
    }
}

