package cn.mikylin.think.ioStrean;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

public class BufferedInputFile {

    public static String read(String filePath) {
        StringBuilder sbf = new StringBuilder();

        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(new File(filePath)));
            String s;
            while ((s = bufferedReader.readLine()) != null) {
                sbf.append(s);
                sbf.append("\n");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
//        System.out.println(sbf.toString());
        return sbf.toString();

    }
}
