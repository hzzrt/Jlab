package cn.mikylin.think.ioStrean;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

public class ChannelCopy {
    private static final int BSIZE=1024;
    public static void main(String [] args) throws Exception{
        FileChannel in = new FileInputStream("D:\\0zrt\\workSpace\\jlab\\jlab-think\\src\\main\\java\\cn\\mikylin\\think\\ioStrean\\ChannelCopy.java").getChannel();
        FileChannel out = new FileOutputStream("text.text").getChannel();
        ByteBuffer bf = ByteBuffer.allocate(BSIZE);
        while (in.read(bf)!=-1){
            bf.flip();
            out.write(bf);
            bf.clear();
        }
    }
}
