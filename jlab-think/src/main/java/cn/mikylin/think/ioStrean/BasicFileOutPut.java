package cn.mikylin.think.ioStrean;

import javax.print.attribute.standard.MediaSize;
import java.io.*;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

public class BasicFileOutPut {
    static String file = "D:\\0zrt\\workSpace\\jlab\\jlab-think\\src\\main\\java\\cn\\mikylin\\think\\ioStrean\\BasicFileOutPut.java";
    static String outFile = "D:\\0zrt\\workSpace\\jlab\\jlab-think\\src\\main\\java\\cn\\mikylin\\think\\ioStrean\\out";

    public static void main(String[] args) throws Exception {
        test4();

    }


    public static void test1() throws Exception {
        BufferedReader in = new BufferedReader(new FileReader(new File(file)));

        PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(outFile)));

        int lineCount = 1;
        String s;
        while ((s = in.readLine()) != null) {
            out.println(lineCount++ + ":" + s);
        }
        out.close();
    }


    public static void test2() throws Exception {
//        BufferedReader in = new BufferedReader(new FileReader(new File(file)));

        LineNumberReader inLne = new LineNumberReader(new FileReader(file));
        PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(outFile)));

        int lineCount = 1;
        String s;
        while ((s = inLne.readLine()) != null) {

            out.println(inLne.getLineNumber() + ":" + s);
        }
        out.close();
    }

    public static void test3() throws Exception {
        BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
        String s;
        List<String> linkedList = new LinkedList<>();
        while ((s = bufferedReader.readLine()) != null) {
            linkedList.add(s);
        }

//        PrintWriter pw = new PrintWriter(outFile);
        FileWriter fileWriter = new FileWriter(outFile);
        for (int i = 0; i < linkedList.size(); i++) {
//            pw.println(i+1+":"+linkedList.get(i));
//            fileWriter.write("\n");
//            fileWriter.flush();
            fileWriter.write(i + 1 + ":" + linkedList.get(i));
            fileWriter.write("\n");
        }
        fileWriter.close();
    }

    public static void test4() throws Exception {


        long t1 = new Date().getTime();
        for(int i =0;i<1000;i++){
            BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
            BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(outFile));
            PrintWriter pw = new PrintWriter(bufferedWriter);

            String s;
            while ((s = bufferedReader.readLine()) != null) {
                pw.println(s);
            }
            pw.close();
            bufferedReader.close();
        }

        System.out.println("buffered out:"+String.valueOf(new Date().getTime()-t1)+"毫秒");



        long t2 = new Date().getTime();
        for(int i=0;i<1000;i++){
            BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
            FileWriter fileWriter = new FileWriter(outFile);
            String s;
            while ((s=bufferedReader.readLine())!=null){
                fileWriter.write(s);
            }
            bufferedReader.close();
            fileWriter.close();
        }
        System.out.println("file out:"+String.valueOf(new Date().getTime()-t2)+"毫秒");

    }
}
