package cn.mikylin.think.ioStrean;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class OSExecute {
    static String charset= "GBK  ";
    public static void command(String command) {

        boolean err = false;
        try {

            Process process = new ProcessBuilder(command.split(" ")).start();
            BufferedReader results = new BufferedReader(
                    new InputStreamReader(process.getInputStream(),charset)
            );
            String s;
            while ((s = results.readLine()) != null) {
                System.out.println(s);
            }
            BufferedReader errors = new BufferedReader(
                    new InputStreamReader(
                            process.getErrorStream(),charset
                    )
            );
            while ((s = errors.readLine()) != null) {
                System.err.println(s);
                err = true;
            }


        } catch (Exception e) {
            if (!command.startsWith("CMD /C"))
                command("CMD /C" + command);
            else
                throw new RuntimeException();
        }
        if (err) {
            throw new OSExecuteException("errors execute" + command);
        }
    }
}


class OSExecuteException extends RuntimeException {
    public OSExecuteException(String why) {
        super(why);
    }
}
