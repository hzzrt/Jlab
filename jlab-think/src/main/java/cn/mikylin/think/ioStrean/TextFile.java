package cn.mikylin.think.ioStrean;

import java.io.*;
import java.lang.reflect.Array;
import java.util.*;

public class TextFile extends ArrayList<String> {

    /**
     * 读取一个文本文件，返回文本字符串
     */
    public static String read(String filePath) throws Exception {
        StringBuilder sbf = new StringBuilder();
        try {
            BufferedReader in = new BufferedReader(new FileReader(new File(filePath).getAbsoluteFile()));
            String s;
            try {
                while ((s = in.readLine()) != null) {
                    sbf.append(s);
                    sbf.append("\n");
                }
            } finally {
                in.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
        return sbf.toString();
    }


    /**
     * 向一个文件，写入text
     */

    public static void write(String filePath, String text) throws Exception {
        try {
            PrintWriter pw = new PrintWriter(new File(filePath).getAbsoluteFile());
            try {
                pw.print(text);
            } finally {
                pw.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }


    public TextFile(String fileName, String splitter) throws Exception {
        super((Arrays.asList(read(fileName).split(splitter))));

        if (get(0).equals(""))
            remove(0);

    }

    public TextFile(String fileName) throws Exception {
        this(fileName, "\n");
    }


    public void write(String fileName) throws Exception {
        try {
            PrintWriter out = new PrintWriter(new File(fileName).getAbsoluteFile());
            try {
                for (String item : this) {
                    out.println(item);
                }
            } finally {
                out.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    public static void main(String[] args) throws Exception {
//        String currentPath = "D:\\0zrt\\workSpace\\jlab\\jlab-think\\src\\main\\java\\cn\\mikylin\\think\\ioStrean\\TextFile.java";
//        String file = read(currentPath);
//        write("test.text", file);
//
//
//        TextFile text = new TextFile("test.text");
//
//
//        text.write("text2.text");
//        text.write("TextFile.java");
//
//
//        TextFile textFile2 = new TextFile("TextFile.java", "\\W+");
//        textFile2.write("test2.text");
//        TreeSet<String> words = new TreeSet<String>(textFile2);
//        System.out.println(words.headSet("a"));

//        test();
        test2();


    }


    public static void test() throws Exception {
        Map<Character, Integer> result = new HashMap<>();
        String str = TextFile.read("aa");
        for (Character c : str.toCharArray()) {
            if (!result.containsKey(c)) {
                result.put(c, 1);
            } else {
                Integer value = result.get(c);
                value++;
                result.put(c, value);
            }

        }

        System.out.println(result);
    }


    public static void test2() throws Exception {
        String filePah = "D:\\0zrt\\workSpace\\jlab\\out\\production\\jlab-think\\cn\\mikylin\\think\\ioStrean";
        Directory.TreeInfo treeInfo = Directory.TreeInfo.walk(filePah);
        List<File> files = treeInfo.files;
        for (File file : files) {
            byte[] bytes = BinaryFile.read(file);
            byte [] caff = Arrays.copyOfRange(bytes,0,4);
            StringBuilder sbf = new StringBuilder();
            for(int i=0;i<caff.length;i++){
                String hex= Integer.toHexString(caff[i]&0xFF);
                if(hex.length() ==1){
                    hex=    '0'+hex;
                }
                sbf.append(hex);
            }
            System.out.println(sbf.toString());
            if(!"cafebabe".equals(sbf.toString())){
                System.out.println("this is not class");
            }
        }
    }

}
