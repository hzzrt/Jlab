package cn.mikylin.think.ioStrean;

import java.io.File;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public final class Directory {
    public static class TreeInfo implements Iterable<File> {

        public List<File> files = new ArrayList<>();
        public List<File> dirs = new ArrayList<>();

        @Override
        public Iterator<File> iterator() {
            return files.iterator();
        }

        void addAll(TreeInfo other) {
            files.addAll(other.files);
            dirs.addAll(other.dirs);
        }


        public static TreeInfo walk(String start, String regex) {
            return recurseDirs(new File(start), regex);

        }
        public static TreeInfo walk(String start){
            return recurseDirs(new File(start),".*");
        }


        /**
         * 递归查询
         */
        static TreeInfo recurseDirs(File statDir, String regex) {
            TreeInfo result = new TreeInfo();
            for (File file : statDir.listFiles()) {
                if (file.isDirectory()) {
                    result.dirs.add(file);
                    result.addAll(recurseDirs(file, regex));
                } else {
                    if (file.getName().matches(regex))
                        result.files.add(file);
                }
            }
            return result;
        }
    }
}
