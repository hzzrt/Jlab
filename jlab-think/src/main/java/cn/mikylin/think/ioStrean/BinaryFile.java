package cn.mikylin.think.ioStrean;

import java.io.*;
import java.nio.channels.FileChannel;
import java.text.Normalizer;

public class BinaryFile {

    /**
     * 读取一个文件的二进制流
     */
    public static byte[] read(File file) throws IOException {
        BufferedInputStream in = new BufferedInputStream(new FileInputStream(file));

        try {
            byte[] data = new byte[in.available()];
            in.read(data);
            return data;
        } finally {
            in.close();
        }

    }


    public static byte[] read(String filePath) throws IOException {
        return read(new File(filePath));
    }


    /**
     * byte数组写入文件中
     */
    public static void write(String destFilePath, byte[] content) throws IOException {
        write(new File(destFilePath), content);
    }

    public static void write(File destFile, byte[] content) throws IOException {
        BufferedOutputStream out = new BufferedOutputStream(new FileOutputStream(destFile));
        try {
            out.write(content);
        } finally {
            out.close();
        }
    }

    /**
     * 二进制文件，从一个文件到另一个文件
     */

    public static void pipe(String originFilePath, String destFilePath) throws IOException {
        write(destFilePath, read(originFilePath));
    }


    /**
     * 使用nio来传输文件
     */
    public static void transferTo(String fromFilePath, String toFilePath) throws Exception {
        FileInputStream from = new FileInputStream(new File(fromFilePath));
        FileOutputStream to = new FileOutputStream(new File(toFilePath));

        FileChannel fromChannel = from.getChannel();
        FileChannel toChannel = to.getChannel();
        fromChannel.transferTo(0, fromChannel.size(), toChannel);
    }


    public static void main(String[] args) throws Exception {
        transferTo("D:\\0zrt\\workSpace\\jlab\\qq.jpg","D:\\0zrt\\workSpace\\jlab\\qq1.jpg");

    }
}
