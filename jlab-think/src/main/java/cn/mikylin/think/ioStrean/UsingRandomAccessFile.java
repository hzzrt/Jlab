package cn.mikylin.think.ioStrean;

import java.io.RandomAccessFile;
import java.nio.charset.Charset;

public class UsingRandomAccessFile {
    static String file = "D:\\0zrt\\workSpace\\jlab\\jlab-think\\src\\main\\java\\cn\\mikylin\\think\\ioStrean\\rtest";

    static void display() throws Exception {
        RandomAccessFile rf = new RandomAccessFile(file, "r");

        for (int i = 0; i < 7; i++) {
            System.out.println("value" + i + ":" + rf.readDouble());
        }
        System.out.println(rf.readUTF());
        rf.close();
    }

    public static void main(String[] args) throws Exception {
        test2();

    }


    static void test2() throws Exception {
        RandomAccessFile rf = new RandomAccessFile(file, "rw");
        rf.writeDouble(1.2323d);
        rf.writeUTF("hello");
        rf.writeBoolean(false);
        rf.writeByte(333);
        rf.writeFloat(33.33f);

        rf.writeChars(new String("yellow".getBytes(),Charset.forName("UTF-8")));
        rf.close();

        RandomAccessFile rf2 = new RandomAccessFile(file, "r");

        System.out.println(rf2.readDouble());
        System.out.println(rf2.readUTF());
        System.out.println(rf2.readBoolean());
        System.out.println(rf2.readByte());
        System.out.println(rf2.readFloat());

        byte [] s =new byte[6];
        rf2.readFully(s);

        System.out.println(new String(s,"Utf-8"));




    }

    static void test1() throws Exception {
        RandomAccessFile rf = new RandomAccessFile(file, "rw");
        for (int i = 0; i < 7; i++) {
            rf.writeDouble(i * 1.414);

        }
        rf.writeUTF("the end of the file");
        rf.close();
        display();


        rf = new RandomAccessFile(file, "rw");
        rf.seek(5 * 8);
        rf.writeDouble(47.0001);
        rf.close();
        display();
    }
}
