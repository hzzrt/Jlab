package cn.mikylin.think.annotation;

import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class TableCreator {
    public static void main(String[] args) throws Exception {
        args = new String[1];

        args[0] = "cn.mikylin.think.annotation.Member";

        for (String className : args) {
            Class<?> cl = Class.forName(className);
            DBTable dbTable = cl.getDeclaredAnnotation(DBTable.class);
            if (dbTable == null) {
                System.out.println("no dbtable annotations in class " + className);
            }


            String tableName = dbTable.name();


            List<String> clumnNames = new ArrayList<>();

            for (Field field : cl.getDeclaredFields()) {
                Annotation[] anns = field.getDeclaredAnnotations();
                if (anns.length < 1) {
                    continue;
                }
                if (anns[0] instanceof SQLInteger) {
                    SQLInteger sInt = (SQLInteger) anns[0];
                    String name = sInt.name();
                    String clumnName = null;

                    if (StringUtils.isNotBlank(name)) {
                        clumnName = name;
                    } else {
                        clumnName = field.getName().toUpperCase();
                    }
                    clumnNames.add(clumnName + " INT " + getConstraint(sInt.constraintsa()));
                }
                if (anns[0] instanceof SQLString) {
                    SQLString sString = (SQLString) anns[0];
                    String name = sString.name();
                    String clumnName = null;
                    if (StringUtils.isNotBlank(name)) {
                        clumnName = name;
                    } else {
                        clumnName = field.getName().toUpperCase();
                    }
                    clumnNames.add(clumnName + " VARCHAR(" + sString.value() + ")" + getConstraint(sString.constraints()));
                }
            }


            StringBuilder creatCommand = new StringBuilder("CREATE TABLE " + tableName + "(");
            for (String columanDef : clumnNames) {
                creatCommand.append("\n   " + columanDef + ",");
            }

            String tableCreate = creatCommand.substring(0, creatCommand.length() - 1) + ");";

            System.out.println(tableCreate);
        }

    }

    private static String getConstraint(Constraints con) {
        StringBuffer sbf = new StringBuffer();
        if (!con.allowNull()) {
            sbf.append("NOT NULL");
        }
        if (con.primaryKey()) {
            sbf.append("PRIMARY KEY");
        }
        if (con.unique()) {
            sbf.append("UNIQUE");
        }
        return sbf.toString();
    }
}
