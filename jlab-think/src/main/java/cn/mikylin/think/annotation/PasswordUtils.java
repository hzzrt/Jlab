package cn.mikylin.think.annotation;

import java.util.List;

public class PasswordUtils {
    @UseCase(id = 47, description = "password must contain at least one numeric")
    public boolean validatePassword(String password) {
        return password.matches("\\w*\\d\\w*");
    }

    @UseCase(id = 48)
    public String encrypPassword(String passsword) {
        return new StringBuilder(passsword).reverse().toString();
    }

    @UseCase(id = 49, description = "new password cant't equal previously used ones")
    public boolean checkForNewPassword(List<String> prevPassswords, String password) {
        return !prevPassswords.contains(password);
    }

}
