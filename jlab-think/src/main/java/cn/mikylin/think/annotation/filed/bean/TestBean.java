package cn.mikylin.think.annotation.filed.bean;

import cn.mikylin.think.annotation.filed.FieldValide;
import cn.mikylin.think.annotation.filed.FieldValideEnum;
import cn.mikylin.think.annotation.filed.FieldValideUtil;
import com.alibaba.fastjson.JSON;

import java.util.Date;
import java.util.List;

public class TestBean {

    private int id;

    @FieldValide(FieldValideEnum.NOTBLANK)
    private String name;

    private String telephone;

    private String email;


    //yyyy-MM-dd HH:mm:ss
    @FieldValide(FieldValideEnum.YYYY_MM_DDHHMMSS)
    private String date;

    private List<String> strings;

    private Date dates;


    public static void main(String[] args) throws Exception {

        TestBean bean = new TestBean();
        bean.setName("1212");
        bean.setId(13123);
        bean.setDate("20190-1-23 12:34:31");
        System.out.println(JSON.toJSONString(FieldValideUtil.valide(bean)));
    }





    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public List<String> getStrings() {
        return strings;
    }

    public void setStrings(List<String> strings) {
        this.strings = strings;
    }

    public Date getDates() {
        return dates;
    }

    public void setDates(Date dates) {
        this.dates = dates;
    }
};
