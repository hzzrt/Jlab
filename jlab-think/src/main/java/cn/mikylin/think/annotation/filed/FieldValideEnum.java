package cn.mikylin.think.annotation.filed;

import org.apache.commons.lang3.StringUtils;

import java.text.SimpleDateFormat;

public enum FieldValideEnum {

    NO_VALIDE(0, "NO_VALIDE", "不做校验") {
        boolean valide(Object o) {
            return true;
        }
    },

    NOTBLANK(1, "notBlank", "不能为空") {
        boolean valide(Object o) {
            if (o instanceof String) {
                return StringUtils.isNoneEmpty((String) o);
            } else {
                return !(o == null);
            }
        }
    },

    YYYYMMDD(2, "YYYYMMDD", "字符串日期格式不符合yyyyMMdd") {
        boolean valide(Object o) {
            if (o instanceof String) {
                String dateStr = (String) o;
                if (StringUtils.isNotBlank(dateStr)) {
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
                    try {
                        sdf.parse(dateStr);
                        return true;
                    } catch (Exception e) {
                        return false;
                    }
                }
            }
            return false;
        }
    },

    YYYY_MM_DDHHMMSS(3, "yyyy-MM-dd HH:mm:ss", "字符串日期格式不符合 yyyy-MM-dd HH:mm:ss") {
        boolean valide(Object o) {
            if (o instanceof String) {
                String dateStr = (String) o;
                if (StringUtils.isNotBlank(dateStr)) {
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    try {
                        sdf.parse(dateStr);
                        return true;
                    } catch (Exception e) {
                        return false;
                    }
                }
            }
            return false;
        }
    },
    YYYYMMDDHHMMSS(4, "yyyyMMddHHmmss", "字符串日期格式不符合 yyyyMMddHHmmss") {
        boolean valide(Object o) {
            if (o instanceof String) {
                String dateStr = (String) o;
                if (StringUtils.isNotBlank(dateStr)) {
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
                    try {
                        sdf.parse(dateStr);
                        return true;
                    } catch (Exception e) {
                        return false;
                    }
                }
            }
            return false;
        }
    },

    TELEPHONE(5,"telePhone","不符合手机号格式"){
        boolean valide(Object o){
            if (o instanceof String) {
                String str = (String) o;
                if (StringUtils.isNotBlank(str)) {
                  if(str.matches("^1(3|4|5|7|8)\\d{9}$")){
                      return true;
                  }
                }
            }
            return false;
        }
    };
























    int id;
    String name;
    String msg;


    abstract boolean valide(Object o);

    FieldValideEnum(int id, String name, String msg) {
        this.id = id;
        this.name = name;
        this.msg = msg;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
