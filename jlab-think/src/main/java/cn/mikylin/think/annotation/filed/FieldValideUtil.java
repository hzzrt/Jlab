package cn.mikylin.think.annotation.filed;

import cn.mikylin.think.annotation.filed.bean.TestBean;
import com.alibaba.fastjson.JSON;


import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;


/**
 * javaBean 字段校验工具类
 */
public class FieldValideUtil {


//    public static Result check(Object bean) {
//        try {
//            Map<String, String> p = valide(bean);
//            String resultCode = p.get("resultCOde");
//            String resultMsg = p.get("resultMsg");
//            if (!"200".equals(resultCode)) {
//                return Result.error(resultCode, resultMsg);
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//            return Result.error("400", "FieldValideUtil.check" + e.getMessage());
//        }
//        return Result.ok();
//    }



    /*提取注解校验*/
    public static Map<String, String> valide(Object bean) throws Exception {
        try {
            Class cls = bean.getClass();
            Annotation[] clsAnns = cls.getDeclaredAnnotations();
            Field[] files = cls.getDeclaredFields();
            for (Field f : files) {
                String name = f.getName();
                f.setAccessible(true);//访问权限
                Object value = f.get(bean);//获取字段的值
                FieldValide[] fieldAnns = f.getAnnotationsByType(FieldValide.class);//拿到字段上的所有注解
                for (FieldValide ann : fieldAnns) {
                    FieldValideEnum enm = ann.value();
                    String msg = enm.msg;
                    StringBuffer sbf = new StringBuffer();
                    if (!enm.valide(value)) {
                        sbf.append("field:" + name + "^value:" + value + msg);
                        return error("400", sbf.toString());
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            return error("409", "FieldValideUtil.valide" + e.getMessage());
        }
        return ok();
    }

    static Map<String, String> error(String code, String msg) {
        Map<String, String> p = new HashMap<>();
        p.put("resultCode", code);
        p.put("resultMsg", msg);
        return p;
    }

    static Map<String, String> ok() {
        Map<String, String> p = new HashMap<>();
        p.put("resultCode", "200");
        p.put("resultMsg", "ok");
        return p;
    }
}

