package cn.mikylin.think.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;

@Target(ElementType.FIELD)
public @interface SQLInteger
{
    String name() default "";

    Constraints constraintsa()default @Constraints;

}
