package cn.mikylin.think.annotation;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


/**
 *
 * 利用反射获取方法上的注解信息
 * UseCase uc = m.getAnnotation(UseCase.class);
* */
public class UseCaseTracker
{
    public static void trackUseCase(List<Integer> useCase,Class<?> cl){
        for(Method m:cl.getDeclaredMethods()){
            UseCase uc = m.getAnnotation(UseCase.class);
            if(uc!=null){
                System.out.println("found use case "+uc.id()+"  "+uc.description());
                useCase.remove(Integer.valueOf(uc.id()));
            }
        }
        for(int i:useCase){
            System.out.println("warning: missing use case-"+i);
        }
    }
    public static void main(String [] args){
        List<Integer> useCase=new ArrayList<Integer>();
        Collections.addAll(useCase,47,48,49,50);
        trackUseCase(useCase,PasswordUtils.class);
    }
}
