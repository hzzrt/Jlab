package cn.mikylin.think.annotation.filed;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface FieldValide {
    FieldValideEnum value() default FieldValideEnum.NO_VALIDE;
}

