package cn.mikylin.think.enums;

import cn.mikylin.think.ioStrean.OSExecute;
import org.eclipse.core.internal.resources.OS;

import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.util.Set;
import java.util.TreeSet;

public class Reflection {


    public static Set<String> analyze(Class<?> enumClass) {
        System.out.println("--------Analyzing " + enumClass + " ---------");
        System.out.println("Interfaces:");
        for (Type t : enumClass.getGenericInterfaces())
            System.out.print(t);

        System.out.println("Base " + enumClass.getSuperclass());
        System.out.print("Method: ");
        Set<String> methods = new TreeSet<>();
        for (Method m : enumClass.getMethods()) {
            methods.add(m.getName());

        }
        System.out.println(methods);
        return methods;
    }

    /**
     * ???enum?????Enum<T>
     *     ????????values()??
     * Compiled from "Reflection.java"
     * final class cn.mikylin.think.enums.Explore extends java.lang.Enum<cn.mikylin.think.enums.Explore> {
     * public static final cn.mikylin.think.enums.Explore HERE;
     * public static final cn.mikylin.think.enums.Explore THERE;
     * public static cn.mikylin.think.enums.Explore[] values();
     * public static cn.mikylin.think.enums.Explore valueOf(java.lang.String);
     * static {};
     * }
     */

    public static void main(String[] args) {
        Set<String> exploreMethods = analyze(Explore.class);
        Set<String> enumMethods = analyze(Enum.class);
        System.out.println("Explore.contansAll(Enum)?" + exploreMethods.containsAll(enumMethods));
        System.out.println("Explore.removeAll(Enum:) ");
        exploreMethods.removeAll(enumMethods);
        System.out.println(exploreMethods);
        OSExecute.command("javap Explore");
    }
}

enum Explore {
    HERE, THERE;
}