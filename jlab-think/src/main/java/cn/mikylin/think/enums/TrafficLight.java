package cn.mikylin.think.enums;

import static cn.mikylin.think.enums.Signals.*;

public class TrafficLight {
    Signals color=Signals.RED;
    public void change(){
        switch(color){
            case RED: color=GREEN;
            break;
            case GREEN:color= YELLOW;
            break;
            case YELLOW:color=RED;
            break;
        }
    }

    public String toString(){
        return "the traffic light is "+color;
    }

    public static void main(String [] args){
        TrafficLight t = new TrafficLight();
        for(int i=0;i<7;i++){
            System.out.println(t);
            t.change();
        }
    }
}
enum Signals{
    GREEN,YELLOW,RED
}