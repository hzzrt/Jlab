package cn.mikylin.think.enums;

public enum  Course {

    APPETIZER(Food.Appetizer.class),
    MAINCOURSE(Food.MainCoutrse.class),
    DESSERT(Food.Dessert.class);

    private Food[] values;

    private Course (Class<? extends Food> kind){
        values = kind.getEnumConstants();
    }
}
