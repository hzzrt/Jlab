package cn.mikylin.think.enums;


//??????????????
public class UpcastEnum {
    public static void main(String[] args) {
        Search[] vals = Search.values();
        Enum en = Search.HERE;

        for (Enum d : en.getClass().getEnumConstants()) {
            System.out.println(d);
        }
    }
}

enum Search {

    HERE, YOU;
}