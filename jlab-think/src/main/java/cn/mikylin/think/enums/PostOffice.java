package cn.mikylin.think.enums;


import org.aspectj.apache.bcel.generic.ReturnaddressType;
import sun.applet.Main;
import sun.java2d.loops.GeneralRenderer;

import java.util.EnumSet;
import java.util.Iterator;

class Mail {
    enum GeneralDelivery {YES, NO1, NO2, NO3, NO4, NO5}

    ;

    enum Scannablitity {UNSCANNABLE, YES1, YES2, YES3, YES4}

    ;

    enum Readablitity {ILLEGIBLE, YES1, YES2, YES3, YES4}

    enum Address {INCORRECT, OK1, OK2, OK3, OK4}

    ;

    enum ReturnAddress {MISSING, OK1, OK2, OK3, OK4, OK5}

    ;
    GeneralDelivery generalDelivery;
    Scannablitity scannablitity;
    Readablitity readablitity;
    Address address;
    ReturnAddress returnaddress;

    static long conter = 0;
    long id = conter++;

    public String toString() {
        return "Mail_" + id;
    }

    public String details() {
        return toString() +
                ",General dekiery :" + generalDelivery +
                ",Address Scanability: " + scannablitity +
                ",Address Readability:" + readablitity +
                ", Address Address:" + address +
                ", returnAddress:" + returnaddress;
    }

    public static Mail randomMail() {
        Mail mail = new Mail();
        mail.generalDelivery = Enums.random(GeneralDelivery.class);
        mail.scannablitity = Enums.random(Scannablitity.class);
        mail.readablitity = Enums.random(Readablitity.class);
        mail.address = Enums.random(Address.class);
        mail.returnaddress = Enums.random(ReturnAddress.class);
        return mail;
    }

    public static Iterable<Mail> generator(final int count) {
        return new Iterable<Mail>() {
            int n = count;

            @Override
            public Iterator<Mail> iterator() {
                return new Iterator<Mail>() {
                    @Override
                    public boolean hasNext() {
                        return n-- > 0;
                    }

                    @Override
                    public Mail next() {
                        return randomMail();
                    }
                };
            }
        };
    }
}


public class PostOffice {
    enum MailHandler {
        GENERAL_DELIVER {
            boolean handle(Mail m) {
                switch (m.generalDelivery) {
                    case YES:
                        System.out.println("using general delivery for " + m);
                        return true;
                    default:
                        return false;
                }
            }
        },
        MACHINE_SCAN {
            boolean handle(Mail m) {
                switch (m.scannablitity) {
                    case UNSCANNABLE:
                        return false;
                    default:
                        switch (m.address) {
                            case INCORRECT:
                                return false;
                            default:
                                System.out.println("delivering " + m + " automiccally");
                                return true;
                        }
                }
            }
        },
        VISUAL_INSPECTION {
            boolean handle(Mail m) {
                switch (m.readablitity) {
                    case ILLEGIBLE:
                        return false;
                    default:
                        switch (m.address) {
                            case INCORRECT:
                                return false;
                            default:
                                System.out.println("Deliver " + m + " normally");
                                return true;
                        }
                }
            }

        },
        RETURN_TO_SENDER {
            boolean handle(Mail m) {
                switch (m.returnaddress) {
                    case MISSING:
                        return false;
                    default:
                        System.out.println("returning " + m + " to sender");
                        return true;
                }
            }
        };

        abstract boolean handle(Mail m);
    }

    static void handle(Mail m) {
        for (MailHandler handler : MailHandler.values()) {
            if (handler.handle(m))
                return;
            System.out.println(m + " is a dead letter");
            return;
        }
    }
        public static void main(String [] args){
        for(Mail mail:Mail.generator(10)){
            System.out.println(mail.details());
            handle(mail);
            System.out.println("+++++++++++++++++++++++++++");
        }
        }
}
