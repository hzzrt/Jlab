package cn.mikylin.think.enums;

public interface Food {
    enum  Appetizer implements Food{
        SALAD,SOUP,SPRING_ROLLS;
    }
    enum MainCoutrse implements Food{
        LASAGNE,BURRITO,PAD_THAI,LENTILS,HUMMOUS
    }

    enum Dessert implements Food{
        TITAMISU,GRLATO,BLACK_FORST_CAKE,FRUIT,CREME_CARAMEL
    }
    enum Coffee implements Food{
        BLACK_COFFEE,DECAF_COFFEE,LATTE,TEA
    }

}
