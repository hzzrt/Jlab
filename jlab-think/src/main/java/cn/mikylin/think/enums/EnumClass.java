package cn.mikylin.think.enums;

public class EnumClass {
    public static void main(String[] args) {
        for(Shrubbery s:Shrubbery.values()){
            System.out.println(s+":ordinary:"+s.ordinal());
            System.out.println(s.compareTo(Shrubbery.CRAWING)+" ");
            System.out.println(s.equals(Shrubbery.CRAWING)+"");
            System.out.println(s==Shrubbery.CRAWING);
            System.out.println(s.getDeclaringClass());
            System.out.println(s.name());
            System.out.println("++++++++++++++++++++++++++++");
        }
        for(String s:"HANGING CRAWING GROUND".split(" ")){
            Shrubbery shrub=Enum.valueOf(Shrubbery.class,s);
            System.out.println(shrub);
        }
    }
}

enum Shrubbery {GROUND, CRAWING, HANGING}
