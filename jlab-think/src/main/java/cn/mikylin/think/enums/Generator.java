//: net/mindview/util/Generator.java
// A generic interface.
package cn.mikylin.think.enums;
public interface Generator<T> { T next(); } ///:~
