package cn.mikylin.think.enums;

import java.util.EnumMap;
interface Command {
    void action();
}
public class EnumMaps
{
    public static void main(String [] args){
        EnumMap<AlarmPoints,Command> em = new EnumMap<AlarmPoints, Command>(AlarmPoints.class);
        em.put(AlarmPoints.KITCHEN, new Command() {
            @Override
            public void action() {
                System.out.println("kitchen cooker");
            }
        });

        em.put(AlarmPoints.BATHROOM, new Command() {
            @Override
            public void action() {
                System.out.println("bathroom wash");
            }
        });

        for(EnumMap.Entry<AlarmPoints,Command> e:em.entrySet()){
            System.out.println(e.getKey()+":");
            e.getValue().action();
        }

        try{
            em.get(AlarmPoints.UTILITY).action();
        }catch (Exception e){
            e.printStackTrace();
        }
    }


}
