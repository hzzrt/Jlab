package cn.mikylin.think.enums;

public enum Meal2 {
    APPETIZER(Food.Appetizer.class),
    MAINCOURSE(Food.MainCourse.class),
    DESSERT(Food.Dessert.class),
    COFFEE(Food.Coffee.class);

    Food[] values;

    private Meal2(Class<? extends Food> kind) {
        values = kind.getEnumConstants();
    }

    public Food randomSelcttion() {
        return Enums.random(values);
    }

    public static void main(String[] args) {
        for (int i = 0; i < 5; i++) {
            for(Meal2 meal:Meal2.values()){
                Food food = meal.randomSelcttion();
                System.out.println(food);
            }
            System.out.println("========");
        }
    }

    public interface Food {
        enum Appetizer implements Food {
            SALAD, SOUP, SPRING_ROLLS
        }

        enum MainCourse implements Food {
            LASAGNE, BUTTION
        }

        enum Dessert implements Food {
            TIRAMISU, GELATO, BLACK_FORST_CAKE,
            FRUIT, CREME_CARAMEL;
        }

        enum Coffee implements Food {
            BLACK_COFFEE, DECAF_COFFEE, EXPRESSO,
            LATTE, TEA;
        }
    }


}
