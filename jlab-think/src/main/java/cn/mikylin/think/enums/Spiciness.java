package cn.mikylin.think.enums;

public enum Spiciness {
    NOT,MILD,MEDIUM,HOT,FLAMING
}
