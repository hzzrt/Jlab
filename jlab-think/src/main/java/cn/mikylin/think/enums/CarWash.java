package cn.mikylin.think.enums;

import java.util.EnumSet;

public class CarWash {
    public enum Cycle {
        UNDERBODY {
            void action() {
                System.out.println("Spraying the underbody");
            }
        },
        WHEELWASH {
            void action() {
                System.out.println("washing the whells");
            }
        },
        PREWASH {
            void action() {
                System.out.println("loosening the dirt");
            }
        },
        BASIC {
            void action() {
                System.out.println("the basic wash");
            }
        },
        HOSTWAX {
            void action() {
                System.out.println("applying hot wax");
            }
        },
        RINSE {
            void action() {
                System.out.println("Rinsing");
            }
        },
        BLOWDRY {
            void action() {
                System.out.println("blowing dry");
            }
        };

        abstract void action();
    }

    EnumSet<Cycle> cycles = EnumSet.of(Cycle.BASIC, Cycle.RINSE);

    public void add(Cycle cycle) {
        cycles.add(cycle);
    }

    public void washCar() {
        for (Cycle c : cycles) {
            c.action();
        }
    }

    public String toString() {
        return cycles.toString();
    }
    public static void main(String [] args){
        CarWash wash = new CarWash();
        System.out.println(wash);
        wash.washCar();
        wash.add(Cycle.BLOWDRY);
        wash.add(Cycle.RINSE);
        wash.add(Cycle.HOSTWAX);
        wash.add(Cycle.BLOWDRY);
        System.out.println(wash);
        wash.washCar();
    }
}
