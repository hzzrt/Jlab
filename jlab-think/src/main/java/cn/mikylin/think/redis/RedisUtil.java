package cn.mikylin.think.redis;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.util.StringUtils;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

import java.util.Collections;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Redis 工具类
 * Created by shuzheng on 2016/11/26.
 */
public class RedisUtil {
//    filter.redis.ip=118.31.22.119
//    filter.redis.password=123456
//    filter.redis.ip=121.196.211.186
//    filter.redis.password=htgf2018
//    filter.redis.port=6379
//    filter.redis.max_active=500
//    filter.redis.max_idle=8
//    filter.redis.max_wait=10000
//    filter.redis.timeout=10000
//    filter.redis.park_db_index=1

    protected static ReentrantLock lockPool = new ReentrantLock();
    protected static ReentrantLock lockJedis = new ReentrantLock();

//    private st logger logger = loggerFactory.getlogger(RedisUtil.class);

    private static Logger logger = LogManager.getLogger(RedisUtil.class);

    // Redis服务器IP
    private static String IP = "118.31.22.119";

    // Redis的端口号
    private static int PORT = 6379;

    //Redis的库编号
    public static int DB_INDEX = 0;

    // 访问密码
    private static String PASSWORD = "123456";

    // 可用连接实例的最大数目，默认值为8；
    // 如果赋值为-1，则表示不限制；如果pool已经分配了maxActive个jedis实例，则此时pool的状态为exhausted(耗尽)。
    private static int MAX_ACTIVE = 500;

    // 控制一个pool最多有多少个状态为idle(空闲的)的jedis实例，默认值也是8。
    private static int MAX_IDLE = 8;

    // 等待可用连接的最大时间，单位毫秒，默认值为-1，表示永不超时。如果超过等待时间，则直接抛出JedisConnectionException；
    private static int MAX_WAIT = 10000;

    // 超时时间
    private static int TIMEOUT = 10000;

    // 在borrow一个jedis实例时，是否提前进行validate操作；如果为true，则得到的jedis实例均是可用的；
    private static boolean TEST_ON_BORROW = false;

    private static JedisPool jedisPool = null;

    /**
     * redis过期时间,以秒为单位
     */
    // 一小时
    public final static int EXRP_HOUR = 60 * 60;
    // 一天
    public final static int EXRP_DAY = 60 * 60 * 24;
    // 一个月
    public final static int EXRP_MONTH = 60 * 60 * 24 * 30;

    /**
     * 初始化Redis连接池
     */
    private static void initialPool() {
        try {
            JedisPoolConfig config = new JedisPoolConfig();
            config.setMaxTotal(MAX_ACTIVE);
            config.setMaxIdle(MAX_IDLE);
            config.setMaxWaitMillis(MAX_WAIT);
            config.setTestOnBorrow(TEST_ON_BORROW);
            jedisPool = new JedisPool(config, IP, PORT, TIMEOUT);
        } catch (Exception e) {
            logger.error("First create IP=[" + IP + "] JedisPool error : ", e);
        }
    }

    /**
     * 在多线程环境同步初始化
     */
    private static synchronized void poolInit() {
        if (null == jedisPool) {
            initialPool();
        }
    }


    /**
     * 同步获取Jedis实例
     *
     * @return Jedis
     */
    public synchronized static Jedis getJedis() {
        poolInit();
        Jedis jedis = null;
        try {
            if (null != jedisPool) {
                jedis = jedisPool.getResource();
                try {
                    jedis.auth(PASSWORD);
                } catch (Exception e) {
                    logger.error("auth jedis ip=[" + IP + "] error : " + e);
                }
            }
        } catch (Exception e) {
            logger.error("Get jedis   ip=[" + IP + "] error : " + e);
        }
        return jedis;
    }

    /**
     * 设置 String
     *
     * @param key
     * @param value
     */
    public synchronized static void set(String key, String value) {
        Jedis jedis = null;
        try {
            value = StringUtils.isEmpty(value) ? "" : value;
            jedis = getJedis();
            jedis.set(key, value);
        } catch (Exception e) {
            logger.error("Set key error : " + e);
        } finally {
            jedis.close();
        }
    }

    /**
     * 设置 String
     *
     * @param key
     * @param value
     */
    public synchronized static void set(int dbIndex, String key, String value) {
        Jedis jedis = null;
        try {
            value = StringUtils.isEmpty(value) ? "" : value;
            jedis = getJedis();
            jedis.select(dbIndex);
            jedis.set(key, value);
        } catch (Exception e) {
            logger.error("Set key error : " + e);
        } finally {
            jedis.close();
        }
    }

    /**
     * 设置 String
     *
     * @param key
     * @param value
     */
    public synchronized static void set(int dbIndex, String key, String value, int seconds) {
        Jedis jedis = null;
        try {
            value = StringUtils.isEmpty(value) ? "" : value;
            jedis = getJedis();
            jedis.select(dbIndex);
            jedis.set(key, value);
            jedis.expire(key, seconds);
        } catch (Exception e) {
            logger.error("Set key error : " + e);
        } finally {
            jedis.close();
        }
    }

    /**
     * 设置 byte[]
     *
     * @param key
     * @param value
     */
    public synchronized static void set(byte[] key, byte[] value) {
        Jedis jedis = null;
        try {
            jedis = getJedis();
            jedis.set(key, value);
        } catch (Exception e) {
            logger.error("Set key error : " + e);
        } finally {
            jedis.close();
        }
    }

    /**
     * 设置 String 过期时间
     *
     * @param key
     * @param value
     * @param seconds 以秒为单位
     */
    public synchronized static void set(String key, String value, int seconds) {
        Jedis jedis = null;
        try {
            value = StringUtils.isEmpty(value) ? "" : value;
            jedis = getJedis();
            jedis.setex(key, seconds, value);
        } catch (Exception e) {
            logger.error("Set keyex error : " + e);
        } finally {
            jedis.close();
        }
    }

    /**
     * 设置 byte[] 过期时间
     *
     * @param key
     * @param value
     * @param seconds 以秒为单位
     */
    public synchronized static void set(byte[] key, byte[] value, int seconds) {
        Jedis jedis = null;
        try {
            jedis = getJedis();
            jedis.set(key, value);
            jedis.expire(key, seconds);
        } catch (Exception e) {
            logger.error("Set key error : " + e);
        } finally {
            jedis.close();
        }
    }

    /**
     * 获取String值
     *
     * @param key
     * @return value
     */
    public synchronized static String get(String key) {
        Jedis jedis = getJedis();
        String value = null;
        if (null == jedis) {
            return null;
        }

        try {
            value = jedis.get(key);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        } finally {
            jedis.close();
        }
        return value;
    }

    /**
     * 获取String值
     *
     * @param key
     * @return value
     */
    public synchronized static String get(int dbIndex, String key) {
        Jedis jedis = getJedis();
        String value = null;
        if (null == jedis) {
            return null;
        }
        try {
            jedis.select(dbIndex);
            value = jedis.get(key);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        } finally {
            jedis.close();
        }
        return value;
    }

    /**
     * 获取byte[]值
     *
     * @param key
     * @return value
     */
    public synchronized static byte[] get(byte[] key) {
        Jedis jedis = getJedis();
        byte[] value = null;
        if (null == jedis) {
            return null;
        }
        try {
            value = jedis.get(key);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        } finally {
            jedis.close();
        }
        return value;
    }

    /**
     * 删除值
     *
     * @param key
     */
    public synchronized static void remove(String key) {
        Jedis jedis = null;
        try {
            jedis = getJedis();
            jedis.del(key);
        } catch (Exception e) {
            logger.error("Remove keyex error : " + e);
        } finally {
            jedis.close();
        }
    }

    /**
     * 删除值
     *
     * @param key
     */
    public synchronized static void remove(int dbIndex, String key) {
        Jedis jedis = null;
        try {
            jedis = getJedis();
            jedis.select(dbIndex);
            jedis.del(key);
        } catch (Exception e) {
            logger.error("Remove keyex error : " + e);
        } finally {
            jedis.close();
        }

    }

    /**
     * 删除值
     *
     * @param key
     */
    public synchronized static void remove(byte[] key) {
        Jedis jedis = null;
        try {
            jedis = getJedis();
            jedis.del(key);
        } catch (Exception e) {
            logger.error("Remove keyex error : " + e);
        } finally {
            jedis.close();
        }
    }

    /**
     * lpush
     *
     * @param key
     * @param key sadd
     */
    public synchronized static void lpush(String key, String... strings) {
        Jedis jedis = null;
        try {
            jedis = RedisUtil.getJedis();
            jedis.lpush(key, strings);
        } catch (Exception e) {
            logger.error("lpush error : " + e);
        } finally {
            jedis.close();
        }
    }

    /**
     * lrem
     *
     * @param key
     * @param count
     * @param value
     */
    public synchronized static void lrem(String key, long count, String value) {
        Jedis jedis = null;
        try {
            jedis = RedisUtil.getJedis();
            jedis.lrem(key, count, value);
        } catch (Exception e) {
            logger.error("lpush error : " + e);
        } finally {
            jedis.close();
        }
    }

    /**
     * @param key
     * @param value
     * @param seconds
     */
    public synchronized static void sadd(String key, String value, int seconds) {
        Jedis jedis = null;
        try {
            jedis = RedisUtil.getJedis();
            jedis.sadd(key, value);
            jedis.expire(key, seconds);
        } catch (Exception e) {
            logger.error("sadd error : " + e);
        } finally {
            jedis.close();
        }
    }

    /**
     * incr
     *
     * @param key
     * @return value
     */
    public synchronized static Long incr(String key) {
        Jedis jedis = getJedis();
        long value = 0;
        if (null == jedis) {
            return null;
        }
        try {
            value = jedis.incr(key);
        } catch (Exception e) {
            logger.error("incr error : " + e);
        } finally {
            jedis.close();
        }
        return value;
    }

    /**
     * decr
     *
     * @param key
     * @return value
     */
    public synchronized static Long decr(String key) {
        Jedis jedis = getJedis();
        long value = 0;
        if (null == jedis) {
            return null;
        }
        try {
            value = jedis.decr(key);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        } finally {
            jedis.close();
        }
        return value;
    }


    /**
     * setnx
     *
     * @param key
     * @return value
     */
    public synchronized static Long setnx(int dbIndex, String key, String value) {
        Jedis jedis = getJedis();
        Long result = 0L;
        if (null == jedis) {
            return null;
        }
        try {
            jedis.select(dbIndex);
            return jedis.setnx(key, value);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        } finally {
            jedis.close();
        }
        return result;
    }


    /**
     * 计数++1
     */
    public synchronized static Long add1(int dbIndex, String key) {
        Jedis jedis = getJedis();
        Long result = 0L;
        if (null == jedis) {
            return null;
        }
        try {
            jedis.select(dbIndex);
            incr(key);
        } catch (Exception e) {
            logger.error("add加1" + e.getMessage(), e);
        } finally {
            jedis.close();
        }
        return result;
    }


    private static final String LOCK_SUCCESS = "OK";
    private static final String SET_IF_NOT_EXIST = "NX";
    private static final String SET_WITH_EXPIRE_TIME = "PX";
    private static final Long RELEASE_SUCCESS = 1L;


    static int REDIS_INDEX = 0;

    /**
     * 尝试获取锁
     * key=加锁的方法名
     * value=uuid
     * expireTime 毫秒
     * 获取锁逻辑：先检查key是否存在redis中，有说明，有其他线程已经获取锁，此次获取锁失败；没有，并set值，获取锁。
     * 检查key是否存在，不存在则set，是个原子操作。
     */

    public static boolean tryGetLock(String lockKey, String requestId, int expireTime) {
        Jedis jedis = getJedis();
        try {

            if (jedis == null) {
                return false;
            }
            String result = jedis.set(lockKey, requestId, SET_IF_NOT_EXIST, SET_WITH_EXPIRE_TIME, expireTime);

            System.out.print(result);
            if (RELEASE_SUCCESS.equals(result)) {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            jedis.close();
        }

        return false;
    }


    /**
     * 释放锁
     */
    public static boolean relaseLock(String lockKey, String requestId) {
        Jedis jedis = getJedis();
        try {

            if (jedis == null) {
                return false;
            }

            String script = "if redis.call('get', KEYS[1]) == ARGV[1] then return redis.call('del', KEYS[1]) else return 0 end";
            Object result = jedis.eval(script, Collections.singletonList(lockKey), Collections.singletonList(requestId));
            if (RELEASE_SUCCESS.equals(result)) {
                return true;
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            jedis.close();
        }
        return false;

    }


    public static void adda(String key) {
        Jedis jedis = getJedis();
        if (jedis == null) {
            return;
        }
        try {
            String script = "redis.call('incr', KEYS[1])";
            Object result = jedis.eval(script, Collections.singletonList(key), Collections.singletonList("0"));
            if (RELEASE_SUCCESS.equals(result)) {
                return;
            }
        } finally {
            jedis.close();
        }


    }

    public  static Long incr(int dbIndex,String key) {
        Jedis jedis = getJedis();
        long value = 0;
        if (null == jedis) {
            return null;
        }
        try {
            jedis.select(dbIndex);
            value = jedis.incr(key);
        } catch (Exception e) {
            logger.error("incr error : " + e);
        } finally {
            jedis.close();
        }
        return value;
    }



    public static void main(String[] args) {
//        tryGetLock("addss", "zrt0917", 100000);
//        relaseLock("addss","zrt0917");

        for (int i = 0; i < 10000; i++) {
            Executors.newFixedThreadPool(1000).execute(new Runnable() {
                @Override
                public void run() {
                    RedisUtil.incr(5,"incr2");
                }
            });
        }
//        set(0,"name" ,"zrt" );
//        System.out.print("end");
//        adda("dasdasd");


    }

}