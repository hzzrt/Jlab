package cn.mikylin.think.thread;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 单线程线程池，所有被提交的任务都是在一个线程中顺序执行
 * */
public class SingleThreadExeutor {
    public static void main(String [] args){
        ExecutorService exec= Executors.newSingleThreadExecutor();
        for(int i=0;i<5;i++){
            exec.execute(new LiftOff());
        }
    }
}
