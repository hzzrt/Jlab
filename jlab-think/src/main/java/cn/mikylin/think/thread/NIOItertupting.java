package cn.mikylin.think.thread;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousCloseException;
import java.nio.channels.ClosedByInterruptException;
import java.nio.channels.SocketChannel;
import java.sql.Time;
import java.util.concurrent.*;

public class NIOItertupting {


}

class NIOBlocked implements Runnable {

    private final SocketChannel sc;

    public NIOBlocked(SocketChannel sc) {
        this.sc = sc;
    }


    @Override
    public void run() {
        try {
            System.out.println("waiting for read() in " + this);
            sc.read(ByteBuffer.allocate(1));
        } catch (ClosedByInterruptException ce) {
            System.out.println("ClosedByInterruptException");
        } catch (AsynchronousCloseException ac) {
            System.out.println("AsynchronousCloseException");
        } catch (IOException ee) {
            throw new RuntimeException(ee);
        }
        System.out.println("exit NIOBlocked run " + this);
    }


    public static void main(String[] args) throws Exception {


        ExecutorService exec = Executors.newCachedThreadPool();

        ServerSocket sc = new ServerSocket(8080);


        InetSocketAddress inetSocketAddress = new InetSocketAddress("localhost", 8080);
        SocketChannel sc1 = SocketChannel.open(inetSocketAddress);
        SocketChannel sc2 = SocketChannel.open(inetSocketAddress);

        Future<?> f = exec.submit(new NIOBlocked(sc1));

        exec.execute(new NIOBlocked(sc2));

        exec.shutdown();

        TimeUnit.SECONDS.sleep(1);

        f.cancel(true);

        TimeUnit.SECONDS.sleep(1);

        sc2.close();

    }
}
