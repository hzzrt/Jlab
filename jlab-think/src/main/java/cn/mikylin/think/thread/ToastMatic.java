package cn.mikylin.think.thread;

import com.sun.org.apache.bcel.internal.generic.RETURN;

import java.util.Random;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

public class ToastMatic {
}

class Toast {
    public enum Status {DRY, BUFFERED, JAMMED}

    private Status state = Status.DRY;
    private final int id;

    public Toast(int id) {
        this.id = id;
    }

    public void buffer() {
        this.state = Status.BUFFERED;
    }

    public void jam() {
        this.state = Status.JAMMED;
    }

    public Status getState() {
        return this.state;
    }

    public int getId() {
        return this.id;

    }

    public String toString() {
        return "Toast" + id + ":" + this.state;
    }
}

class ToastQueue extends LinkedBlockingQueue<Toast> {

}

class Toaster implements Runnable {
    private ToastQueue toastQueue;
    private int count = 0;
    private Random rand = new Random(47);

    public Toaster(ToastQueue queue) {
        this.toastQueue = queue;
    }

    public void run() {
        try {
            while (!Thread.interrupted()) {
                TimeUnit.MILLISECONDS.sleep(100 + rand.nextInt(500));
                Toast t = new Toast(count++);
                System.out.print(t);
                toastQueue.put(t);
            }
        } catch (InterruptedException e) {
            System.out.println("toaster interrupted");
        }
        System.out.println("toaster off");
    }
}

class Butterer implements Runnable {
    public void run() {

    }
}