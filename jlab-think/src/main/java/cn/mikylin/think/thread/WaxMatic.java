package cn.mikylin.think.thread;

import com.sun.xml.internal.bind.v2.model.annotation.RuntimeAnnotationReader;

import java.sql.Time;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;



class Car {
    private  boolean waxOn = false;

    public synchronized void waxed() {
        waxOn = true;
        notifyAll();
    }

    public synchronized void buffed() {
        waxOn = false;
        notifyAll();
    }


    public synchronized void waitForWaxing() throws InterruptedException {
        while (waxOn == false) {
            wait();
        }
    }


    public synchronized void waitForBuffering() throws InterruptedException {
        while (waxOn == true) {
            wait();
        }
    }
}


class Waxon implements Runnable {
    private Car car;

    public Waxon(Car c) {
        this.car = c;
    }

    public void run() {
        try {
            while (!Thread.interrupted()) {
                System.out.println("wax on!");
                TimeUnit.MILLISECONDS.sleep(200);
                car.waxed();
                car.waitForBuffering();
            }
        } catch (InterruptedException e) {
            System.out.println("exit interrupt");
        }

        System.out.println("ending wax on task");
    }
}


class Waxoff implements Runnable {
    private Car car;

    public Waxoff(Car c) {
        this.car = c;
    }

    public void run() {
        try {

            while (!Thread.interrupted()) {
                car.waitForBuffering();
                System.out.println("wax off");
                TimeUnit.MILLISECONDS.sleep(200);
                car.buffed();
            }
        } catch (InterruptedException e) {
            System.out.println("exiting via interrupt");
        }

        System.out.println("ending wax off task");
    }
}


/**
 *
 * 通过wait()和notifyAll()保证了线程任务间的先后顺序。*/
public class WaxMatic {
    public static void main(String[] args) throws Exception {
        Car car = new Car();
        ExecutorService exec = Executors.newCachedThreadPool();
        exec.execute(new Waxoff(car));
        exec.execute(new Waxon(car));
        TimeUnit.SECONDS.sleep(5);
        exec.shutdownNow();
    }
}
