package cn.mikylin.think.thread;

import java.sql.Timestamp;
import java.util.concurrent.TimeUnit;


//后台线程，后台线程依附于非后台线程。所有的非后台线程执行完了以后，后台线程会被戛然而止。
public class Daemon implements Runnable {
    @Override
    public void run() {
        try{
        System.out.println("String Adaem");
            TimeUnit.SECONDS.sleep(1);
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            System.out.println("finally exec");
        }

    }
    public static void main(String [] args){
        Thread t= new Thread(new Daemon());
//        t.setDaemon(true);
        t.start();
    }
}
