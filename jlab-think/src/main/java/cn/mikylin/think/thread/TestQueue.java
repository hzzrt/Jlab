package cn.mikylin.think.thread;

import java.util.concurrent.*;

public class TestQueue {
    public static void main(String[] args) {
        LinkedBlockingQueue<String> queue = new LinkedBlockingQueue<>();
        ExecutorService exec = Executors.newCachedThreadPool();
        for(int i=0;i<10;i++){
            exec.execute(new Product(queue, String.valueOf(i)));
        }


        for(int i=0;i<10;i++){
            exec.execute(new Consume(queue ));
        }

    }
}

class Consume implements Runnable {
    private BlockingQueue<String> queue;

    public Consume(BlockingQueue<String> queue) {
        this.queue = queue;
    }

    public void run() {
        try {
            System.out.println("take:" + queue.take());
        } catch (InterruptedException e) {
            System.out.println("interrupt");
        }
        return ;
    }
}


class Product implements Runnable {
    private BlockingQueue<String> queue;

    public Product(BlockingQueue<String> queue, String msg) {
        this.queue = queue;
        this.msg = msg;
    }

    private String msg;

    public void run() {
        try {
            System.out.println("put:" + msg);
            queue.put(msg);
        } catch (InterruptedException e) {
            System.out.println("interrupt");
        }
        return;
    }
}
