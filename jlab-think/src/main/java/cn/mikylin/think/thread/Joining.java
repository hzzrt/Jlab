package cn.mikylin.think.thread;

import com.sun.prism.shader.Solid_TextureYV12_Loader;
import com.sun.xml.internal.ws.api.message.ExceptionHasMessage;

/**
 * t.join 阻塞当前线程，直到线程t完成，才继续此线程
 * */
public class Joining {

    public static void main(String [] args){
//        Sleeper sleey = new Sleeper("Sleepy",1500);
//        Sleeper grumy = new Sleeper("Grumpy",1500);
//        Joiner dopey  = new Joiner("Dopey",sleey);
//        Joiner doc = new Joiner("Doc",grumy);
//        grumy.isInterrupted();

        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
               throw new RuntimeException();

            }
        });
        t.start();
    }

}
class Sleeper extends Thread{
    private int duration;
    public Sleeper (String name,int sleepTime){
        super(name);
        duration=sleepTime;
        start();
    }
    public void run(){
        try{
            sleep(duration);
        }catch (InterruptedException e){
            System.out.println(getName()+"was interrupted"+"is interrupted()"+isInterrupted());
            return;
        }
        System.out.println(getName()+"has awakened");
    }
}

class Joiner extends Thread{
    private Sleeper sleeper;
    public Joiner(String name,Sleeper sleeper){
        super(name);
        this.sleeper=sleeper;
        start();
    }
    public void run(){
        try{
            sleeper.join();

        }catch (InterruptedException e){
            System.out.println("interrupted");
        }

        System.out.println(getName()+" join completed");
    }
}