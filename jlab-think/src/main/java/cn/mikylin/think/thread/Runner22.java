package cn.mikylin.think.thread;

import java.sql.Time;
import java.util.Date;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.TimeUnit;


class Run1 implements Runnable {
    private  Runner22 r22;

    public Run1(Runner22 r22) {
        this.r22 = r22;
    }

    public void run() {

        System.out.println("run1 run");

        try {
            TimeUnit.SECONDS.sleep(5);

            this.r22.flag = true;
            System.out.println("flag:"+true);

            synchronized (this.r22){
                this.r22.notify();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}


class Run2 implements Runnable {
    private  Runner22 r22;

    public Run2(Runner22 r22) {
        this.r22 = r22;
    }

    public void run() {

        long time1 = new Date().getTime();
        while (!this.r22.flag) {
            try {
                synchronized (this.r22) {
                    this.r22.wait();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        long time3  = new Date().getTime();
        System.out.println("awaken ");
//        long time1 = new Date().getTime();
////        synchronized (this) {
//    while (true){
//        if(Runner22.flag==true){
//            Runner22.flag=false;
//            long time = (new Date().getTime()-time1)/1000;
//            System.out.println("r22==false"+"耗时："+time+"秒");
//        }
//
//    }


//        }
    }
}

public class Runner22 {
    public  boolean flag = false;


    public static void main(String[] args) {
        Runner22 r22 = new Runner22();
        ExecutorService exec = Executors.newCachedThreadPool();
        exec.execute(new Run1(r22));
        exec.execute(new Run2(r22));
    }
}
