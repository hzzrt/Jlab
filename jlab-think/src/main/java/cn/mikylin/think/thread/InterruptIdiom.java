package cn.mikylin.think.thread;

import java.util.concurrent.TimeUnit;


class NeedCleanUp {
    private final int id;

    public NeedCleanUp(int id) {
        this.id = id;
        System.out.println("NeedCleanUp" + id);

    }

    public void cleanUp() {
        System.out.println("clean " + id);
    }

}

class Blocked3 implements Runnable {
    private volatile double d = 0.0;


    public void run() {

        try {
            while (!Thread.currentThread().isInterrupted()) {
                NeedCleanUp n1 = new NeedCleanUp(1);
                try {
                    System.out.println("sleeping");
                    TimeUnit.SECONDS.sleep(1);
                    NeedCleanUp n2 = new NeedCleanUp(2);

                    try {
                        System.out.println("calculating");
                        for (int i = 1; i < 25000; i++) {
                            System.out.println("finished time comsume operation");
                        }
                    } finally {
                        n2.cleanUp();
                    }
                } finally {

                    n1.cleanUp();
                }
            }

            System.out.println("exiting via while ()test");
        } catch (Exception e) {

            System.out.println("exiting via interruptException");
        }

    }
}


public class InterruptIdiom {

    public static void main(String[] args) throws Exception {
        Thread t1 = new Thread(new Blocked3());

        t1.start();

        TimeUnit.SECONDS.sleep(1);

        System.out.println("issue interrupt");

        t1.interrupt();
    }

}