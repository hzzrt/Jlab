package cn.mikylin.think.thread;


import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

class SleepBlocked implements Runnable {
    public void run() {
        try {
            TimeUnit.SECONDS.sleep(100);
        } catch (InterruptedException e) {
            System.out.println("InterruptedException ");
        }
        System.out.println("exit sleepBlock RUN");
    }
}


class IoBlocked implements Runnable {
    private InputStream io;

    public IoBlocked(InputStream io) {
        this.io = io;
    }

    public void run() {
        try {
            System.out.println("wait for read");
            io.read();
        } catch (IOException e) {
            if (Thread.currentThread().isInterrupted()) {
                System.out.println("interrupt from  block io");
            } else {
                throw new RuntimeException(e);
            }
        }
        System.out.println("exit ioBlocked run");
    }
}

class SynchronizedBlocked implements Runnable {
    public synchronized void f() {
        while (true) {
            Thread.yield();
        }
    }

    public SynchronizedBlocked() {
        new Thread() {
            public void run() {
                f();
            }
        }.start();
    }

    public void run() {
        System.out.println("trying to call f()");
        f();
        System.out.println("exit synchronizedBlocked run ");
    }
}

public class Interrupting {

    private static ExecutorService exec = Executors.newCachedThreadPool();

    static void test(Runnable r) throws InterruptedException {

        Future<?> f = exec.submit(r);
        TimeUnit.MILLISECONDS.sleep(100);
        System.out.println("interrupting " + r.getClass().getSimpleName());
        f.cancel(true);
        System.out.println("interrupt sent to " + r.getClass().getSimpleName());
    }

    public static void main(String[] args) throws Exception {
        test(new SleepBlocked());
        test(new IoBlocked(System.in));
        test(new SynchronizedBlocked());
        TimeUnit.SECONDS.sleep(3);
        System.out.println("aborting exit");
        System.exit(0);




    }
}
