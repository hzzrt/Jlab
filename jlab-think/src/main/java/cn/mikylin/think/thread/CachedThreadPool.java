package cn.mikylin.think.thread;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * cachedThreadPoll 可缓存的线程池，任务数量超过当前线程数量，则新建线程；线程数量超过处理需要，则回收空闲线程
 * */
public class CachedThreadPool {
    public static void main(String [] args){
        ExecutorService exec = Executors.newCachedThreadPool();
        for(int i=0;i<5;i++){
            exec.execute(new LiftOff());
        }
        exec.shutdown();//停止接收新任务
        System.out.println("shut down");
        for(int i=0;i<5;i++){
            exec.execute(new LiftOff());
        }
    }
}
