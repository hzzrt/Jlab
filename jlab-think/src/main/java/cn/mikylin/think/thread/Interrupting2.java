package cn.mikylin.think.thread;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;


class BlockedMutex {
    private Lock lock = new ReentrantLock();

    public BlockedMutex() {
        lock.lock();
    }

    public void f() {
        try {
            lock.lockInterruptibly();
        } catch (InterruptedException e) {
            System.out.println("interruptException from lock in f()");
        }
    }
}

class Blocked2 implements Runnable {
    private BlockedMutex mutex = new BlockedMutex();

    public void run() {
        System.out.println("wait for blocked ");
        mutex.f();
        System.out.println("end");

    }

}

public class Interrupting2 {
    public static void main(String[] args) throws Exception {

        Thread t1 = new Thread(new Blocked2());
        t1.start();

        TimeUnit.SECONDS.sleep(2);


        System.out.println("issue interrupt");
        t1.interrupt();


    }


}