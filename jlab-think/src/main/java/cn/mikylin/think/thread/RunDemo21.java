package cn.mikylin.think.thread;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;



class Runner1 implements Runnable {
    public void run() {
        try {
            wait11();
        } catch (InterruptedException e) {
            System.out.println("InterruptedException  11");
        }

    }

    public synchronized void wait11() throws InterruptedException {
        wait();
        System.out.println("awaken runner1 run!");
    }
}


class Runner2 implements Runnable {
    private Runner1 runner1;

    public Runner2(Runner1 r1) {
        this.runner1 = r1;
    }

    public void run() {
        try {

            System.out.println("runner2 run!");
            TimeUnit.SECONDS.sleep(3);
          synchronized (runner1){
              runner1.notifyAll();
          }
        } catch (InterruptedException e) {
            System.out.println("InterruptedException 22");
        }

    }
}


public class RunDemo21 {
    public static  void main(String [] args) throws InterruptedException{
        ExecutorService exec= Executors.newCachedThreadPool();
        Runner1 r1 = new Runner1();
        exec.execute(r1);
        exec.execute(new Runner2(r1));

    }
}