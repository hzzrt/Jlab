package cn.mikylin.think.thread;

public class EvenGenerator extends IntGenerator {

    private int currentEvenValue = 0;

    @Override
    public synchronized int next() {
        ++currentEvenValue;
//        Thread.yield();
        ++currentEvenValue;
        return currentEvenValue;
    }

    @Override
    public void cancel() {
        super.cancel();
    }

    @Override
    public boolean isCanceled() {
        return super.isCanceled();
    }

    public static void main(String [] args){
        EvenChecker.test(new EvenGenerator());
    }



}
