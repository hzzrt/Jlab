package cn.mikylin.think.thread;

import java.io.InputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class CloseRecource {
    public static void main(String[] args) throws Exception {
        ExecutorService exec = Executors.newCachedThreadPool();
        ServerSocket ser = new ServerSocket(8080);

        InputStream socketinput = new Socket("localhost", 8080).getInputStream();
        exec.execute(new IoBlocked(socketinput));
        exec.execute(new IoBlocked(System.in));
        TimeUnit.SECONDS.sleep(1);
        System.out.println("shutdowm all thread");

        exec.shutdown();

        TimeUnit.SECONDS.sleep(3);

        System.out.println("close " + socketinput.getClass().getSimpleName());
        socketinput.close();

        TimeUnit.SECONDS.sleep(1);
        System.out.println("close " + System.in.getClass().getSimpleName());
        System.in.close();

    }
}
