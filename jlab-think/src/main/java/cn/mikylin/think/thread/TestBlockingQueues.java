package cn.mikylin.think.thread;

import javax.management.ObjectName;
import java.util.List;
import java.util.concurrent.*;

public class TestBlockingQueues {
    public static void main(String[] args) {
        test("LinkedBlockingQueue", new LinkedBlockingQueue());
        test("ArrayBlockingQueue", new ArrayBlockingQueue(100));
        test("SynchronousQueue", new SynchronousQueue());
//        test("DelayQueue", new DelayQueue());
    }
    static void test(String msg,BlockingQueue queue){
        System.out.println("test msg");




        LiftOffRunner lfr = new LiftOffRunner(queue);


        Thread t = new Thread(lfr);
        t.start();

        System.out.println("t.start()");

        try{
            for(int i=0;i<2;i++){
                lfr.add(new LiftOff(3));
            }
            TimeUnit.SECONDS.sleep(2);
        }catch (Exception e){
            System.out.println("interrupt ");
        }


        t.interrupt();


        System.out.println("finished "+msg+",test");
    }

}

class LiftOffRunner implements Runnable {
    private BlockingQueue<LiftOff> rockets;

    public LiftOffRunner(BlockingQueue<LiftOff> rockets) {
        this.rockets = rockets;
    }

    public void add(LiftOff lf) {
        try {
            rockets.put(lf);
        } catch (InterruptedException e) {
            System.out.println("interrupt when add queue");
        }

    }

    public void run() {
        try {
            while (!Thread.interrupted()) {
                LiftOff lf = rockets.take();
                lf.run();
            }

        } catch (InterruptedException e) {
            System.out.println("interrupt when take");
        }
        System.out.println("exiting liftoffRunner");
        return;

    }


}
