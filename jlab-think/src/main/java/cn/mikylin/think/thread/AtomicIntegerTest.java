package cn.mikylin.think.thread;

import java.util.concurrent.atomic.AtomicInteger;

public class AtomicIntegerTest implements Runnable {

    private AtomicInteger i = new AtomicInteger();

    public int getValue() {
        return i.get();
    }

    private void evenIncrement() {
        i.addAndGet(1);
    }

    @Override
    public void run() {
        while (true) {
            evenIncrement();
        }
    }
}
